workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == "main" || $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - when: never

variables:
  IMAGE_NAME: ${CI_REGISTRY_IMAGE}
  DEV_SERVER_USERNAME: ${DEV_SERVER_USERNAME}
  DEV_SERVER_HOST: ${DEV_SERVER_HOST}
  DEV_SSH_KEY: ${SSH_PRIVATE_KEY}
  STAGING_SERVER_USERNAME: ${DEV_SERVER_USERNAME}
  STAGING_SERVER_HOST: ${DEV_SERVER_HOST}
  STAGING_SSH_KEY: ${SSH_PRIVATE_KEY}
  PRODUCTION_SERVER_USERNAME: ${DEV_SERVER_USERNAME}
  PRODUCTION_SERVER_HOST: ${DEV_SERVER_HOST}
  PRODUCTION_SSH_KEY: ${SSH_PRIVATE_KEY}

stages:
  - test
  - build
  - deploy_dev
  - deploy_staging
  - deploy_production

run_unit_tests:
  stage: test
  tags:
    - linux
    - aws
    - docker
  image: node:21-alpine
  cache:
    key: "${CI_COMMIT_REF_NAME}"
    paths:
      - app/node_modules
    policy: pull-push
  before_script:
    - cd app
    - npm install
  script:
    - npm test
  artifacts:
    when: always
    paths:
      - app/junit.xml
    reports:
      junit: app/junit.xml

run_lint_tests:
  stage: test
  tags:
    - linux
    - aws
    - docker
  image: node:21-alpine
  cache:
    key: "${CI_COMMIT_REF_NAME}"
    paths:
      - app/node_modules
    policy: pull
  before_script:
    - cd app
    - npm install
  script:
    - echo "Running lint checks..."

sast:
  stage: test

create_image_tag:
  stage: build
  tags:
    - linux
    - aws
    - docker
  image: alpine
  script:
    - apk add --no-cache jq
    - PACKAGE_JSON_VERSION=$(cat app/package.json | jq -r .version)
    - VERSION=$PACKAGE_JSON_VERSION.$CI_PIPELINE_IID
    - echo $VERSION >> version-file.txt
  artifacts:
    paths:
      - version-file.txt

build_image:
  stage: build
  needs:
    - create_image_tag
  tags:
    - linux
    - aws
    - docker
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [ "" ]
  before_script:
    - export VERSION=$(cat version-file.txt)
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${IMAGE_NAME}:${VERSION}"

.deploy:
  tags:
    - linux
    - aws
    - docker
  image: ubuntu
  variables:
    SSH_KEY: ""
    SERVER_HOST: ""
    SERVER_USERNAME: ""
    DEPLOY_ENV: ""
    APP_PORT: ""
    ENDPOINT: ""
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - eval $(ssh-agent -s)
    - echo "${SSH_KEY}" | base64 -d | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Run this below if the host isn't changing regularly
    # - cp "$SSH_KNOWN_HOSTS" ~/.ssh/known_hosts # SSH_KNOWN_HOSTS is a variable of type file
    - ssh-keyscan ${SERVER_HOST} >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - export VERSION=$(cat version-file.txt)
  script:
    - scp ./docker-compose.yaml "${SERVER_USERNAME}@${SERVER_HOST}:/home/${SERVER_USERNAME}"
    - ssh "${SERVER_USERNAME}@${SERVER_HOST}" "
      docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} &&
      
      export COMPOSE_PROJECT_NAME=${DEPLOY_ENV}
      export DC_IMAGE_NAME=${IMAGE_NAME} &&
      export DC_IMAGE_TAG=${VERSION} &&
      export DC_APP_PORT=${APP_PORT}
      
      docker compose down &&
      docker compose up -d"
  environment:
    name: ${DEPLOY_ENV}
    url: "${ENDPOINT}"

deploy_to_dev:
  extends: .deploy
  stage: deploy_dev
  variables:
    SSH_KEY: "${DEV_SSH_KEY}"
    SERVER_HOST: "${DEV_SERVER_HOST}"
    SERVER_USERNAME: "${DEV_SERVER_USERNAME}"
    DEPLOY_ENV: development
    APP_PORT: 3000
    ENDPOINT: "http://${DEV_SERVER_HOST}:3000"

run_functional_tests:
  stage: deploy_dev
  needs:
    - deploy_to_dev
  script:
    - echo "Running functional tests..."

deploy_to_staging:
  extends: .deploy
  stage: deploy_staging
  variables:
    SSH_KEY: "${STAGING_SSH_KEY}"
    SERVER_HOST: "${STAGING_SERVER_HOST}"
    SERVER_USERNAME: "${STAGING_SERVER_USERNAME}"
    DEPLOY_ENV: staging
    APP_PORT: 4000
    ENDPOINT: "http://${STAGING_SERVER_HOST}:4000"

run_performance_tests:
  stage: deploy_staging
  needs:
    - deploy_to_staging
  script:
    - echo "Running performance tests..."

deploy_to_production:
  extends: .deploy
  stage: deploy_production
  variables:
    SSH_KEY: "${PRODUCTION_SSH_KEY}"
    SERVER_HOST: "${PRODUCTION_SERVER_HOST}"
    SERVER_USERNAME: "${PRODUCTION_SERVER_USERNAME}"
    DEPLOY_ENV: production
    APP_PORT: 5000
    ENDPOINT: "http://${PRODUCTION_SERVER_HOST}:5000"
  when: manual

include:
  template: "Jobs/SAST.gitlab-ci.yml"
